package test.data_structures;

import org.junit.Before;

import junit.framework.TestCase;
import model.data_structures.Stack;

public class TestStack extends TestCase {
	
	
	private Stack<String> pila; 
	
	
	@Before
	public void setupEscenario()
	{
		pila = new Stack<String>();
		pila.push("Hola");
		pila.push("Como");
		pila.push("Estas");
		pila.push("Bien");
		pila.push("Chido");
		
	}
	public void testPop()
	{
		setupEscenario();
		assertEquals("Error al a�adir un archivo", "Chido",pila.pop());
		
	}
	public void testPush()
	{
		setupEscenario();
		String a�adir = "OK"; 
		pila.push(a�adir);
		assertEquals("Error al retornar usar pop en la lista", "OK",pila.pop());
	}
	

}
