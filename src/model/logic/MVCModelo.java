package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import com.opencsv.CSVReader;

import model.data_structures.Stack;
import model.data_structures.Queue;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo 
{
	/**
	 * Atributos del modelo del mundo
	 */


	private Stack <Viaje> datos1 ; 

	private Queue <Viaje> datos2 ;
	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		datos1 = new Stack <Viaje>();
		datos2 = new Queue <Viaje>();
	}



	/**
	 * Servicio de consulta de numero de elementos presentes en el modelo 
	 * @return numero de elementos presentes en el modelo
	 */
	public int darTamano()
	{
		return datos1.size();
	}

	/**
	 * Requerimiento de agregar dato
	 * @param dato
	 */
	public void agregar(Viaje dato)
	{	
		datos1.push(dato);
		datos2.enqueue(dato);
	}

	/**
	 * Requerimiento buscar dato
	 * @param dato Dato a buscar
	 * @return dato encontrado
	 */



	public int cargar() {
		int rta =0;

		CSVReader reader = null;
		try 
		{

			reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-1-All-HourlyAggregate.csv"));
			String[] nextLine=reader.readNext();
			while ((nextLine = reader.readNext()) != null) 
			{
				Viaje elem = new Viaje(Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), 
						Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]),
						Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), 
						Double.parseDouble(nextLine[6]));

				agregar(elem);

				rta++;



			}



		} 

		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
		finally
		{
			if (reader != null) 
			{
				try 
				{
					reader.close();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}

		return rta;
	}


	public Queue <Viaje> darClusterDeViajePorHora(int h)
	{   


		ArrayList <Queue<Viaje>> list=  new ArrayList <Queue<Viaje>> ();
		while (datos2.isEmpty())
		{
			if (datos2.dequeue().getHod()>=h)
			{Queue<Viaje> cola = new Queue <Viaje>();
			Viaje comp = datos2.dequeue();
			cola.enqueue(comp);

			boolean para= false;
			while (para )
			{
				if(comp.getHod()>datos2.dequeue().getHod() )
				{
					para = true;
				}
				else 
				{ Viaje add= datos2.dequeue();
				cola.enqueue(add);
				comp= add;
				}
			}
           list.add(cola);
			}

		}

		Queue<Viaje> max= list.get(0);
		for ( int i= 0; i<list.size(); i++)
		{ if(list.get(i).size()> max.size())
		{
			max= list.get(i);
		}	
		}
		return max;
	}
	public Queue <Viaje> nUltimosViajes(int n , int h)
	{   Queue<Viaje> cola = new Queue <Viaje>();

	while( datos1.isEmpty()|| cola.size()==n)
	{

		if (datos1.pop().getHod()==h)
		{
			Viaje add = datos1.pop();
			cola.enqueue(add);
		}

	}
	return cola;
	}
}