package model.data_structures;

import java.util.Iterator;

public class Stack<T> implements IStack<T> 
{

	private Nodo<T> primero; 

	private int peso; 

	public Stack()
	{
		primero = null;
	}


	public T darPrimero()
	{
		return primero.darElemento();
	}
	public Iterator<T> iterator() 
	{
		// TODO Auto-generated method stub
		return new IteratorLista(primero);
	}


	public boolean isEmpty() 
	{
		// TODO Auto-generated method stub
		return primero == null;
	}


	public int size() 
	{
		// TODO Auto-generated method stub
		return peso;
	}


	public void push(T t) {
		// TODO Auto-generated method stub
		Nodo<T> nuevo = new Nodo<T>(t);
		if(primero == null)
		{
			primero = nuevo; 
			peso++;
		}
		else
		{
			nuevo.cambiarSiguiente(primero);
			primero = nuevo;
			peso++;
		}

	}


	public T pop() {
		// TODO Auto-generated method stub
		Nodo<T> retornar = primero; 
		if(peso == 1)
		{
			primero = null; 
			peso--;
			return retornar.darElemento();
		}
		else
		{ 
			primero = primero.darSiguiente();
			peso--;
			return retornar.darElemento();
		}

	}
}
